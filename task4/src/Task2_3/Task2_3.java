
package Task2_3;

import java.util.ArrayList;
import java.util.Collections;
import java.util.InputMismatchException;
import java.util.Scanner;


public class Task2_3 {
    public static void main(String[] args) {
        ArrayList<Integer> s = new ArrayList<>();
        Scanner sc = new Scanner(System.in);
        System.out.print("Number of element = ");
        int x = sc.nextInt();
        sc.nextLine();
        try{
           for(int i = 0 ; i < x ; i++){
               System.out.print("s = ");
               int x1 = sc.nextInt();
               sc.nextLine();
               s.add(x1);
           }
            
        }catch(InputMismatchException ex){
            System.out.println("Please! import Integer"+ex.getMessage());
        }catch(Exception e){
            System.out.println(e.getMessage());
        }
        Collections.sort(s);
        Collections.reverse(s);
        //task2
        System.out.println("Task 2 : ");
        for(int i = 0; i < s.size(); i++){
            System.out.print(s.get(i)+" ");
        }
        //task3
        System.out.println("\nTask 3 : ");
        System.out.print("Number max k = ");
        int k = sc.nextInt();
        sc.nextLine();
         for(int i = 0; i < k; i++){
            System.out.print(s.get(i)+" ");
        }
        
        
    }

}
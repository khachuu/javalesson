
package Task1_2_3;

import java.util.ArrayList;
import java.util.Collections;
import java.util.InputMismatchException;
import java.util.Scanner;


public class Shuffle {

    
    public static void main(String[] args) {

            Scanner sc = new Scanner(System.in);
            ArrayList<Integer> a = new ArrayList<>();
            int n ;
            int x;
            System.out.print("number of element x = ");
            x = sc.nextInt();

            try{
                 for(int i = 0; i < x ; i++){
                System.out.print("element = ");
                n = sc.nextInt();
                a.add(n);
//                Exception ix = new InputMismatchException();
//                throw ix;
                 }

            }catch(InputMismatchException e){
                System.out.println("Import Integer!"+e.getMessage());
            }catch(Exception e){
                System.out.println(e.getMessage());
            }
          
            System.out.println("a = "+a);
            //task1 
            Collections.shuffle(a);
            System.out.println("task1.1 a = "+a);
            //task2
            Collections.sort(a);
            System.out.println("task2.1 a = "+a);
            //task3.1
            Collections.reverse(a);
            System.out.println("task3.1 a = "+a);
            MyStack m = new MyStack(5);
            try{
                for(int i = 1; i < 5; i++){
                    System.out.print("oneofstack = ");
                    int t = sc.nextInt();
                        m.push(t);
                }
            }catch(InputMismatchException e){
                System.out.println("Import Integer!"+e.getMessage());
            }catch(Exception e){
                System.out.println("fail"+e.getMessage());}
            
           //task3.2
           a.clear();
            while(!m.isEmpty()){
                int value = m.pop();
                
                a.add(value);
            }
            System.out.println("a = "+a);
           
            }
            
    }
    

